Ansible module used by OSAS to install and start firewalld.

This module has no configuration options, and just install and start firewalld,
in order to be usable by all the roles that do require it.
